# Documentation 

* If you are not familiar with Markdown, check the [Markdown help](https://gitlab.com/sfuclimate/uvic-model-2.9/wikis/Help-Markdown)
* If you want to use UVic on the CEDAR server, check the [cheatsheet CEDAR](https://gitlab.com/sfuclimate/uvic-model-2.9/wikis/Cheatsheet-Cedar)
* Our wiki is a separate repository. If you want to get access to the wiki from your terminal and change it, you can clone it by taping `git clone https://gitlab.com/sfuclimate/uvic-model-2.9.wiki.git`
